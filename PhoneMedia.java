package com.javarush.task.pro.task10.taskPhoneConnection;

public interface PhoneMedia {
   String  TakePhoto();
   String  TakeVideo();
}
