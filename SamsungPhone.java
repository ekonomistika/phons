package com.javarush.task.pro.task10.taskPhoneConnection;

 class SamsungPhone implements PhoneConnection, PhoneMedia {

     @Override
     public int Messedge() {

         return 1;
     }

     @Override
     public int Call() {
         return 2;
     }

     @Override
     public String TakePhoto() {
         return "Фото Самсунг";
     }

     @Override
     public String TakeVideo() {
         return "Видео Самсунг";
     }
 }
