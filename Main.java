package com.javarush.task.pro.task10.taskPhoneConnection;

public class Main {
    public static void main(String[] args) {

        NokiaPhone nokiaPhone = new NokiaPhone();
        SamsungPhone samsungPhone = new SamsungPhone();

        System.out.println(nokiaPhone.TakePhoto());
        System.out.println(nokiaPhone.TakeVideo());
        System.out.println(samsungPhone.TakePhoto());
        System.out.println(nokiaPhone.TakeVideo());
    }
}
