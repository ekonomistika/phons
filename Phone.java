package com.javarush.task.pro.task10.taskPhoneConnection;

public abstract class Phone {
    public  char name;
    public  char model;
    public  int valueOZY;
    public  int valueStore;

    public Phone(char name, char model, int valueOZY, int valueStore){
        this.name = name;
        this.model = model;
        this.valueOZY = valueOZY;
        this.valueStore = valueStore;
    };

}
