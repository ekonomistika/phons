package com.javarush.task.pro.task10.taskPhoneConnection;

public class NokiaPhone implements PhoneMedia{

    @Override
    public String TakePhoto() {
        return "Фото Нокиа";
    }

    @Override
    public String  TakeVideo() {
        return "Видео Нокиа";
    }
}
